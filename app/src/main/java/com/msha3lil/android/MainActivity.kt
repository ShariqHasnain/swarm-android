package com.msha3lil.android

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.msha3lil.android.ui.*
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.msha3lil.android.ui.dialog.MoreCheckingDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.home_toolbar.*


class MainActivity : AppCompatActivity() {
    companion object {
        var status = 0
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUi()
    }
    private fun initUi() {

        // bottomNavigationView.menu.getItem(2).isEnabled = false
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            selectFragment(item)
            true
        }
        //Always load first fragment as default
        val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout, HomeFragment())
        fragmentTransaction.commit()
        bottomNavigationView.background = null

        ivMsg.setOnClickListener {
            startActivity(Intent(this, MsgActivity::class.java))
        }
        llUser.setOnClickListener {
            var intent=Intent(this, SettingActivity::class.java)
            intent.putExtra("Setting",true)
            startActivity(intent)
        }
        fabLocation.setOnClickListener {
            startActivity(Intent(this, CheckPlaceActivity::class.java))
        }
        etSearch.setOnClickListener {

            startActivity(Intent(this, SearchActivity::class.java))
        }
    }
    private fun selectFragment(item: MenuItem) {
        var fragment: Fragment? = null
        val fragmentClass: Class<*>
        fragmentClass = when (item.getItemId()) {
            R.id.user -> HomeFragment::class.java
            else -> GroupFragment::class.java
        }
        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val fragmentManager: FragmentManager = supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).commit()
        }
    }
    override fun onStart() {
        super.onStart()
        val mLocationRequest = LocationRequest.create()
        mLocationRequest.interval = 60000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mLocationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        //TODO: UI updates.
                    }
                }
            }
        }
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        LocationServices.getFusedLocationProviderClient(this)
            .requestLocationUpdates(mLocationRequest, mLocationCallback, null)
    }

    override fun onResume() {
        super.onResume()
        if(status==1){

            var  dialog = MoreCheckingDialog()
           // var  dialog = CheckoutDialog()

            dialog.show(supportFragmentManager, "ViewPrescriptionDialog")
            status=0
        }
    }

}