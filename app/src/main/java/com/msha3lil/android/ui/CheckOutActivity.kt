package com.msha3lil.android.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.msha3lil.android.MainActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.ItemClickListener
import com.msha3lil.android.ui.adapter.TagFriendAdapter
import com.msha3lil.android.utils.extention.visible
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import kotlinx.android.synthetic.main.activity_check_out.*


class CheckOutActivity : AppCompatActivity() {
    private var adapter: TagFriendAdapter? = null
    private val photoPaths = ArrayList<String>()
    val tagList = arrayListOf<String>()
    val TAG_ACTIVITY = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_out)
        initUi()
    }

    private fun initUi() {

        ivTagFriend.setOnClickListener {

            openActivityForResult()
        }
        tvCheckIn.setOnClickListener {

            //    var  dialog = CheckoutCoinsDialog()
            MainActivity.status = 1
            finish()


            //    startActivity(Intent(this, DialogActivity::class.java))
            // finish()
        }
        ivImageAttach.setOnClickListener {
            onPicPhoto()
        }

        adapter = TagFriendAdapter(tagList, object : ItemClickListener {
            override fun clicked(data: String) {
            }

        })
        rvFriendsTag.adapter = adapter
    }

    fun openActivityForResult() {
        startForResult.launch(Intent(this, CheckInFriendActivity::class.java))
    }


    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                intent?.let {
                    val list = it.getStringArrayListExtra("friendsList")
                    list?.let { friends ->
                        if (friends.isNotEmpty()) {
                            if (list.size == 1) {
                                tvTag.text = friends[0]
                                llTag.visibility = View.VISIBLE
                            } else if (friends.size > 1) {
                                llTag.visibility = View.GONE
                                tvTag.text = ""

                            }
                            tagList.clear()
                            tagList.addAll(friends)
                            adapter?.notifyDataSetChanged()
                        }
                    }
                }
            }
        }

    fun onPicPhoto() = runWithPermissions(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) {
        FilePickerBuilder.instance
            .setMaxCount(1)
            .setSelectedFiles(photoPaths)
            .setActivityTheme(R.style.appTheme)
            .setActivityTitle("Please select media")
            .enableVideoPicker(false)
            .enableCameraSupport(true)

            .showGifs(false)
            .showFolderView(true)
            .enableSelectAll(false)
            .enableImagePicker(true)
            .withOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
            .pickPhoto(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            FilePickerConst.REQUEST_CODE_PHOTO -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    tvImageCount.visible()
                }
            }
        }
    }

}