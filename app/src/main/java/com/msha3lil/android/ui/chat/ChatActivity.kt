package com.msha3lil.android.ui.chat

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_signup.*
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList


class ChatActivity : AppCompatActivity() {
    private lateinit var madapter: ConversationRecyclerView
    private var mchatData: ArrayList<ChatData> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        mchatData = setData()
        madapter =  ConversationRecyclerView(this, mchatData)
        rlConversation.adapter = madapter
//        rlConversation.postDelayed(Runnable {
//            rlConversation.smoothScrollToPosition(
//                (rlConversation.getAdapter()?.getItemCount() ?: 1) - 1
//            )
//        }, 1000)


        //    rlConversation.smoothScrollToPosition(5);
        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
        ivCrossDialog.setOnClickListener {
            rlDialog.visibility = View.GONE
        }

        editor.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                val text = editor.text.toString().trim()
                if(text.isNotEmpty()) {
                    sendMessage(text)
                }
                true
            } else false
        })

        ivImage.setOnClickListener{
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
        }
    }

    private fun sendMessage(text: String) {
        val chatData = ChatData("2", text, "Just Now", null)
        mchatData.add(0, chatData)
        madapter.notifyDataSetChanged()
        rlConversation.scrollToPosition(0)
        editor.text.clear()
    }

    fun setData(): ArrayList<ChatData> {
        val list: ArrayList<ChatData> = ArrayList()
        val text = arrayOf(
            "",
            "Ok!",
            "Yes! Coffe maybe?",
            "Great idea! You can come 9:00 pm? :)))",
            "I'm fine. Wanna go out somewhere?",
            "Hi, Shariq! How are you?"
        )
        val time = arrayOf(
            "Just now",
            "Just now",
            "5 min. ago",
            "9 min. ago",
            "9 min. ago",
            "10 min. ago"
        )
        val type = arrayOf("4", "1", "2", "2", "1", "2")
        for (i in text.indices) {
            val item = ChatData()
            item.setType(type[i])
            item.setText(text[i])
            item.setTime(time[i])
            list.add(item)
        }
        return list
    }


    val PICK_IMAGE = 1

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            var selectedImageUri: Uri? = null
            if (data != null) {
                selectedImageUri = data.data
                val `in`: InputStream?
                try {
                    `in` = contentResolver.openInputStream(selectedImageUri!!)
                    val selectedImageBitmap = BitmapFactory.decodeStream(`in`)
                    sendMessage(selectedImageBitmap)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "An error occured!", Toast.LENGTH_LONG).show()
                }
            }


        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun sendMessage(bitmap: Bitmap) {
        val chatData = ChatData("4", "", "Just Now", bitmap)
        mchatData.add(0, chatData)
        madapter.notifyDataSetChanged()
        rlConversation.scrollToPosition(0)
        editor.text.clear()
    }

}