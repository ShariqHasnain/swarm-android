package com.msha3lil.android.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.ui.PageActivity
import com.msha3lil.android.ui.SearchActivity
import com.msha3lil.android.ui.SearchDetailActivity
import com.msha3lil.android.ui.models.Place

class PlacesAdapter(private val places: ArrayList<Place>,private val sectionNumber: Int) :
    RecyclerView.Adapter<PlacesAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val placeTitle: TextView = view.findViewById(R.id.placeTitle)
        val placeDescription: TextView = view.findViewById(R.id.placeDescription)
        val placeImageView: ImageView = view.findViewById(R.id.placeImage)
        val numberOfCheckIns: TextView = view.findViewById(R.id.numberOfCheckins)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.place_item, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.placeTitle.text = places[position].placeTitle
        viewHolder.placeDescription.text = places[position].placeDescription
        viewHolder.numberOfCheckIns.text = "${places[position].placeDescription} Check-In"
        viewHolder.itemView.setOnClickListener{
            if(sectionNumber == 1)
                viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context,PageActivity::class.java))
            else
                viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context,SearchDetailActivity::class.java))
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = places.size
}