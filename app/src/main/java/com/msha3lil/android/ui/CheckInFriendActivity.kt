package com.msha3lil.android.ui

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.FriendCheckAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import kotlinx.android.synthetic.main.activity_check_in_friend.*
import kotlinx.android.synthetic.main.simple_toolbar.*
import android.app.Activity

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import java.util.ArrayList


class CheckInFriendActivity : AppCompatActivity() {
    private var adapter: FriendCheckAdapter? = null
    val friendsList = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in_friend)
        initUi(this)
    }

    private fun initUi(context: Context) {
        tvTbTitle.text = "Check in your friends..."

        etSearch.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                adapter?.filter?.filter(s?.trim())
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        adapter = FriendCheckAdapter(
            arrayListOf("M. Shariq Hasnain", "Zeeshan", "Adnan"),
            object : ItemClickListener {
                override fun clicked(data: String) {
                    friendsList.add(data)
                    doneLayout.visibility = View.VISIBLE
                    val distinctFriends = friendsList.distinct()
                    if(distinctFriends.size == 1)
                        friendsCountTv.text = "${distinctFriends[0]}"
                    else
                        friendsCountTv.text = "${distinctFriends.size} Friend selected"
                }
            })
        rlFriends.adapter = adapter

        doneBtn.setOnClickListener {
            val returnIntent = Intent()
            val friendsListUnique = arrayListOf<String>()
            friendsListUnique.addAll(friendsList.distinct())
            returnIntent.putStringArrayListExtra("friendsList", friendsListUnique)
            setResult(RESULT_OK, returnIntent)
            finish()
        }
        ivBack.setOnClickListener {
            finish()
        }
    }
}