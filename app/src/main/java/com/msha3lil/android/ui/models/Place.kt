package com.msha3lil.android.ui.models

data class Place(val placeTitle: String, val placeDescription: String, val placeImageUrl: String, val numberOfCheckins: Int)