package com.msha3lil.android.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.NonNull
import androidx.fragment.app.DialogFragment
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.DailogRankAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import com.msha3lil.android.ui.adapter.RankFriendsAdapter
import kotlinx.android.synthetic.main.dialog_leaderboard.*


class LeaderboardDialog : DialogFragment() {
    @NonNull
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                Color
                    .TRANSPARENT
            )
        )
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_leaderboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomUp: Animation = AnimationUtils.loadAnimation(
            activity,
            R.anim.bounch
        )

        rlCard.startAnimation(bottomUp)
        ivClose.setOnClickListener {
            dismiss()
        }
        rlFriends.adapter= DailogRankAdapter(arrayOf("","",""),object : ItemClickListener {
            override fun clicked(data: String) {


            }
        })

    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }
}