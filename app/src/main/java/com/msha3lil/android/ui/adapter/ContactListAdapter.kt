package com.msha3lil.android.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.ui.models.Contact

class ContactListAdapter(private val contacts: ArrayList<Contact>) :
    RecyclerView.Adapter<ContactListAdapter.ViewHolder>(), Filterable {
    var contactListFiltered = contacts

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val contactName: TextView = view.findViewById(R.id.contactName)
        val contactNumber: TextView = view.findViewById(R.id.contactNumber)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_contact, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.contactName.text = contactListFiltered[position].name
        viewHolder.contactNumber.text = contactListFiltered[position].number
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = contactListFiltered.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint?.toString() ?: ""
                if (charString.isEmpty()) contactListFiltered = contacts else {
                    val filteredList = arrayListOf<Contact>()
                    contacts
                        .filter {
                            (it.name.contains(constraint!!, true)) or
                                    (it.number.contains(constraint, true))

                        }
                        .forEach { filteredList.add(it) }
                    contactListFiltered = filteredList

                }
                return FilterResults().apply { values = contactListFiltered }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                contactListFiltered = if(results?.values == null) arrayListOf()
                else
                    results.values as ArrayList<Contact>
                notifyDataSetChanged()
            }
        }
    }
}