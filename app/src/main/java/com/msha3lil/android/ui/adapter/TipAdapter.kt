package com.msha3lil.android.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.utils.extention.gone
import com.msha3lil.android.utils.extention.visible

class TipAdapter(private val dataSet: Array<String>) :
    RecyclerView.Adapter<TipAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val upvote: TextView

        init {
            // Define click listener for the ViewHolder's View.
            upvote = view.findViewById(R.id.tvUpvote)

        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_tip, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        //  viewHolder.textView.text = dataSet[position]

        viewHolder.apply {
            upvote.setOnClickListener {
                upvote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vote_green, 0, 0, 0);
                upvote.setTextColor(Color.GREEN)
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}