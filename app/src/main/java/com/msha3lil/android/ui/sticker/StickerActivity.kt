package com.msha3lil.android.ui.sticker

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.simple_toolbar.*
import kotlinx.android.synthetic.main.simple_toolbar.ivBack
import kotlinx.android.synthetic.main.simple_toolbar.tvTbTitle

class StickerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sticker)

        tvTbTitle.text="Sticker Book"
        val sectionsPagerAdapter = StickerPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
    }
}