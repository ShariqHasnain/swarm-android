package com.msha3lil.android.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R

class CommentAdapter(
    private val dataSet: ArrayList<String>,
) :
    RecyclerView.Adapter<CommentAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val commenterTv: TextView = view.findViewById(R.id.commenter)
        val commentTextTv: TextView = view.findViewById(R.id.commentText)
        val timeTv: TextView = view.findViewById(R.id.time)

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_comment, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.commenterTv.text = "M. Shariq Hasnain"
        viewHolder.commentTextTv.text = dataSet[position]
        viewHolder.timeTv.text = "Just Now"

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}