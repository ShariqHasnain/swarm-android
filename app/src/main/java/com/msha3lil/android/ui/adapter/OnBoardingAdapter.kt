package com.msha3lil.android.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.data.OnBoardingItem


class OnBoardingAdapter(onBoardingItems: List<OnBoardingItem>) :
    RecyclerView.Adapter<OnBoardingAdapter.OnboardingViewHolder>() {
    private val onBoardingItems: List<OnBoardingItem> = onBoardingItems
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnboardingViewHolder {
        return OnboardingViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_container_boarding, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: OnboardingViewHolder, position: Int) {
        holder.setOnBoardingData(onBoardingItems[position])
    }

    override fun getItemCount(): Int {
        return onBoardingItems.size
    }

    inner class OnboardingViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val textTitle: TextView
        private val textDescription: TextView
        private val imageOnboarding: ImageView
        fun setOnBoardingData(onBoardingItem: OnBoardingItem) {
            textTitle.setText(onBoardingItem.title)
            textDescription.setText(onBoardingItem.description)
            imageOnboarding.setImageResource(onBoardingItem.image)
        }

        init {
            textTitle = itemView.findViewById(R.id.textTitle)
            textDescription = itemView.findViewById(R.id.textDescription)
            imageOnboarding = itemView.findViewById(R.id.imageOnboarding)
        }
    }

}
