package com.msha3lil.android.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.msha3lil.android.R
import com.msha3lil.android.utils.extention.visible
import droidninja.filepicker.FilePickerBuilder
import kotlinx.android.synthetic.main.activity_check_out.*
import kotlinx.android.synthetic.main.activity_page.ivBack
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_write_tip.*
import java.io.FileNotFoundException
import java.io.InputStream

class WriteTipActivity : AppCompatActivity() {
    private var photoPaths = ArrayList<String>()
    val PICK_IMAGE = 1
    val TAG_ACTIVITY = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_tip)
        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
        tvAttach.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
        }

        tvTbSave.setOnClickListener {
            finish()
        }
    }
    fun openActivityForResult() {
        startForResult.launch(Intent(this, CheckInFriendActivity::class.java))
    }


    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            llTag.visible()
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                // Handle the Intent
                //do stuff here
            }
        }

    fun onPicPhoto() = runWithPermissions(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) {
        FilePickerBuilder.instance
            .setMaxCount(1)
            .setSelectedFiles(photoPaths)
            .setActivityTheme(R.style.appTheme)
            .setActivityTitle("Please select media")
            .enableVideoPicker(false)
            .enableCameraSupport(true)

            .showGifs(false)
            .showFolderView(true)
            .enableSelectAll(false)
            .enableImagePicker(true)
            .withOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
            .pickPhoto(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            var selectedImageUri: Uri? = null
            if (data != null) {
                selectedImageUri = data.data
                val `in`: InputStream?
                try {
                    `in` = contentResolver.openInputStream(selectedImageUri!!)
                    val selected_img = BitmapFactory.decodeStream(`in`)
                    imgAttach.setImageBitmap(selected_img)
                    tvAttach.visibility = View.GONE
                    txtAttach.visibility = View.GONE
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "An error occured!", Toast.LENGTH_LONG).show()
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}