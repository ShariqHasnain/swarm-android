package com.msha3lil.android.ui.models

data class Contact(val name: String, val number: String)
