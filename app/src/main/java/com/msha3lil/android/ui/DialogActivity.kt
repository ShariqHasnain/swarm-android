package com.msha3lil.android.ui

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.msha3lil.android.R
import com.msha3lil.android.utils.extention.visible
import kotlinx.android.synthetic.main.activity_dialog.*
import kotlinx.android.synthetic.main.activity_msg.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import pl.droidsonroids.gif.GifDrawable


class DialogActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog)

        val gifDrawable = gif.drawable as GifDrawable
        gifDrawable.stop()
        val bottomUp: Animation = AnimationUtils.loadAnimation(
            this,
            R.anim.bounch
        )

        val slideDown: Animation = AnimationUtils.loadAnimation(
            this,
            R.anim.slide_down_full
        )
        llTop.startAnimation(bottomUp)
        llTop.visibility = View.VISIBLE

        lifecycleScope.launch {
            delay(2000)
            gif.visible()
            llTop.startAnimation(slideDown)
            llTop.visibility = View.GONE
            lifecycleScope.launch {
                delay(500)
                finish()
            }
        }

        initUi()
    }
    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
    }
}