package com.msha3lil.android.ui.chat;

import android.graphics.Bitmap;

/**
 * Created by Dytstudio.
 */

public class ChatData {
    public ChatData() {
    }

    public ChatData(String type, String text, String time, Bitmap image) {
        this.type = type;
        this.text = text;
        this.time = time;
        this.imageBitmap = image;
    }

    String type, text, time;
    Bitmap imageBitmap;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }
}
