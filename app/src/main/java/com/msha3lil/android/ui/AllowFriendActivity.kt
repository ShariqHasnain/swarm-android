package com.msha3lil.android.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_allow_friend.*

class AllowFriendActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_allow_friend)

        tvCancel.setOnClickListener(View.OnClickListener {
            clTop.visibility = View.GONE
        })

        tvCheckIn.setOnClickListener(View.OnClickListener {
            clTop.visibility = View.GONE
        })
    }
}