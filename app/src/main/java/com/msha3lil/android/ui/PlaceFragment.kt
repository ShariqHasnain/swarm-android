package com.msha3lil.android.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.PlacesAdapter
import com.msha3lil.android.ui.models.Place
import kotlinx.android.synthetic.main.fragment_place.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"


class PlaceFragment : Fragment() {

    var sectionNumber: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            sectionNumber = it.getInt(ARG_SECTION_NUMBER)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_place, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populatePlaces()
    }

    private fun populatePlaces() {
        val places = arrayListOf<Place>()
        places.add(Place("Test", "1 place", "", 4))
        places.add(Place("Test", "1 place", "", 4))
        places.add(Place("Test", "1 place", "", 1))
        rvPlaces.adapter = PlacesAdapter(places, sectionNumber)

    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceFragment {
            return PlaceFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }

    }
}