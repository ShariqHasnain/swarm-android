package com.msha3lil.android.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_check_in_streak.*

class CheckInStreakActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in_streak)

        ivBack.setOnClickListener {
            finish()
        }
    }
}