package com.msha3lil.android.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.ui.models.Contact

class FriendListAdapter(
    private val dataSet: ArrayList<String>,
    private val listener: ItemClickListener
) :
    RecyclerView.Adapter<FriendListAdapter.ViewHolder>(),Filterable {
    var datasetFiltered = dataSet
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.tvTitle)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_friend, viewGroup, false)
        view.setOnClickListener {
            listener.clicked("")
        }
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
          viewHolder.textView.text = datasetFiltered[position]
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = datasetFiltered.size


    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint?.toString() ?: ""
                if (charString.isEmpty()) datasetFiltered = dataSet else {
                    val filteredList = arrayListOf<String>()
                    dataSet
                        .filter {
                            (it.contains(constraint!!, true))

                        }
                        .forEach { filteredList.add(it) }
                    datasetFiltered = filteredList

                }
                return FilterResults().apply { values = datasetFiltered }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                datasetFiltered = if(results?.values == null) arrayListOf()
                else
                    results.values as ArrayList<String>
                notifyDataSetChanged()
            }
        }
    }

}