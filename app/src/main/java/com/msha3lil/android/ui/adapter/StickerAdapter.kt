package com.msha3lil.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.msha3lil.android.R


class StickerAdapter(context: Context, courseModelArrayList: Array<String>) :
    ArrayAdapter<String>(context, 0, courseModelArrayList) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var listitemView = convertView
        if (listitemView == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            listitemView = LayoutInflater.from(context).inflate(R.layout.sticker_list, parent, false)
        }
//        val courseModel: String? = getItem(position)
//        val courseTV = listitemView!!.findViewById<TextView>(R.id.idTVCourse)
//        val courseIV = listitemView.findViewById<ImageView>(R.id.idIVcourse)
//        courseTV.setText(courseModel.getCourse_name())
//        courseIV.setImageResource(courseModel.getImgid())
        return listitemView!!
    }
}