package com.msha3lil.android.ui

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.*
import kotlinx.android.synthetic.main.activity_check_in.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.activity_search_detail.*
import kotlinx.android.synthetic.main.fragment_place.*
import kotlinx.android.synthetic.main.home_toolbar.*
import kotlinx.android.synthetic.main.simple_toolbar.*
import java.text.SimpleDateFormat
import java.util.*

class SearchDetailActivity : AppCompatActivity(), ItemClickListener {
    private val myCalendar: Calendar = Calendar.getInstance()
    lateinit var fromDate: OnDateSetListener
    lateinit var toDate: OnDateSetListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_detail)
        tvTbTitle.text = "Four Seasons Hotel"
        clickListener()
        myCalendar.time = Date()
        if((myCalendar.time).compareTo(Date()) == 0){
            txt_to_date.text = "Today"
        }

        val adapter = PlaceCheckInAdapter(arrayOf(
            "Four Seasons Hotel",
            "Four Seasons Hotel",
            "Four Seasons Hotel",
            "Four Seasons Hotel",
            "Four Seasons Hotel")
        )
        rvCheckIn.adapter = adapter
    }

    private fun clickListener(){
        from_calendar.setOnClickListener(View.OnClickListener {
            DatePickerDialog(
                this@SearchDetailActivity,
                fromDate,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            ).show()
        })

        to_calendar.setOnClickListener(View.OnClickListener {
            DatePickerDialog(
                this@SearchDetailActivity,
                toDate,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            ).show()
        })

        fromDate =
            OnDateSetListener { view, year, month, day ->
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = month
                myCalendar[Calendar.DAY_OF_MONTH] = day
                updateLabelFromDate(calendar = myCalendar)
            }

        toDate =
            OnDateSetListener { view, year, month, day ->
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = month
                myCalendar[Calendar.DAY_OF_MONTH] = day
                updateLabelToDate(calendar = myCalendar)
            }

        ivBack.setOnClickListener { finish() }

    }

    private fun updateLabelToDate(calendar: Calendar) {
        val myFormat = "MM/dd/yy"
        val dateFormat = SimpleDateFormat(myFormat, Locale.US)
        if(dateFormat.format(calendar.time).compareTo(dateFormat.format(Date())) == 0) {
            txt_to_date.text = "Today"
        } else {
            txt_to_date.text = dateFormat.format(calendar.time)
        }
    }

    private fun updateLabelFromDate(calendar: Calendar) {
        val myFormat = "MM/dd/yy"
        val dateFormat = SimpleDateFormat(myFormat, Locale.US)
        txt_from_date.text = dateFormat.format(calendar.time)
    }

    override fun clicked(data: String) {
        startActivity(Intent(this, VenueDetailActivity::class.java))
    }

}