package com.msha3lil.android.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_msg.*
import kotlinx.android.synthetic.main.simple_toolbar.*
import kotlinx.android.synthetic.main.simple_toolbar.ivBack
import kotlinx.android.synthetic.main.simple_toolbar.tvTbTitle

class MayorshipsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mayorships)

         tvTbTitle.text="Mayorships"
        initUi()
    }
    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
    }
}