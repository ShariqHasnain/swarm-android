package com.msha3lil.android.ui.category

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.fragment_category_all.*
import org.eazegraph.lib.models.PieModel


/**
 * A placeholder fragment containing a simple view.
 */
class CategoryAllFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_category_all, container, false)


        return root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val pie = AnyChart.pie()
//
//        val data: MutableList<DataEntry> = ArrayList()
//        data.add(ValueDataEntry("John", 10000))
//        data.add(ValueDataEntry("Jake", 12000))
//        data.add(ValueDataEntry("Peter", 18000))
//
//        piechart.setChart(pie)
        piechart.addPieSlice(PieModel("Clothing Store", 9F, resources.getColor(R.color.orange)))
        piechart.addPieSlice(PieModel("Work", 1F, resources.getColor(R.color.white)))

        piechart.startAnimation()

        var adapter= CategoryAdapter(arrayOf("Mashroom", "Kitkat", "Oreo", "Lolipop", "", ""))
        rlCategory.adapter=adapter

    }
    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): CategoryAllFragment {
            return CategoryAllFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }



}