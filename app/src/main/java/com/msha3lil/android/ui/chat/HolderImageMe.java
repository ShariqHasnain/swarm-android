package com.msha3lil.android.ui.chat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.msha3lil.android.R;


/**
 * Created by Dytstudio.
 */

public class HolderImageMe extends RecyclerView.ViewHolder {

    private ImageView imageView;

    public HolderImageMe(View v) {
        super(v);
        imageView = (ImageView) v.findViewById(R.id.ivImage);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

}
