package com.msha3lil.android.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.utils.extention.gone
import com.msha3lil.android.utils.extention.invisible
import com.msha3lil.android.utils.extention.visible
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.title_toolbar.*


class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
        btnLogIn.setOnClickListener {
            startActivity(Intent(this, IntroActivity::class.java))
        }

        etEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (s.isNotEmpty()) {
                    showEmailViews(true)
                } else {
                    showEmailViews(false)
                }
            }
        })
        etPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (s.isNotEmpty()) {
                    showNumberViews(true)
                } else {
                    showNumberViews(false)
                }
            }
        })
    }

    private fun showEmailViews(flag: Boolean) {
        if (flag) {
            etPassword.visible()
            btnLogIn.visible()
            tvforgot.visible()
            tvDescTop.gone()
            llPhone.invisible()
        } else {
            etPassword.gone()
            btnLogIn.gone()
            tvforgot.gone()
            llPhone.visible()
            tvDescTop.visible()
        }
    }

    private fun showNumberViews(flag: Boolean) {
        if (flag) {
            tvDescNum.visible()
            btnNext.visible()
            etEmail.gone()
            tvDescTop.invisible()
        } else {
            tvDescNum.gone()
            btnNext.gone()
            etEmail.visible()
            tvDescTop.visible()
        }
    }

}