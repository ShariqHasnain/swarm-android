package com.msha3lil.android.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.ui.PageActivity
import com.msha3lil.android.ui.models.Contact

class CheckPlaceAdapter(
    private val dataSet: ArrayList<String>,
    private val listener: ItemClickListener?
) :
    RecyclerView.Adapter<CheckPlaceAdapter.ViewHolder>(), Filterable {
    var dataSetFiltered = dataSet

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        //        val textView: TextView
        val title: TextView = view.findViewById(R.id.tvTitle)
        val info: ImageView = view.findViewById(R.id.ivInfo)

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_check_place, viewGroup, false)
        view.setOnClickListener {
            listener?.clicked("")
        }

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.apply {
            title.text = dataSetFiltered[position]
        }
        viewHolder.info.setOnClickListener {
            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context,PageActivity::class.java))
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSetFiltered.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint?.toString() ?: ""
                if (charString.isEmpty()) dataSetFiltered = dataSet else {
                    val filteredList = arrayListOf<String>()
                    dataSet
                        .filter {
                            (it.contains(constraint!!, true))

                        }
                        .forEach { filteredList.add(it) }
                    dataSetFiltered = filteredList

                }
                return FilterResults().apply { values = dataSetFiltered }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                dataSetFiltered = if(results?.values == null) arrayListOf()
                else
                    results.values as ArrayList<String>
                notifyDataSetChanged()
            }
        }
    }

}