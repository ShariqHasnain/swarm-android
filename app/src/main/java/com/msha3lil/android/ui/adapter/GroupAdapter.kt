package com.msha3lil.android.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.ui.VenueDetailActivity
import com.msha3lil.android.utils.extention.gone
import com.msha3lil.android.utils.extention.visible

class GroupAdapter(private val dataSet: Array<String>, private val context: Context) :
    RecyclerView.Adapter<GroupAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val likeCount: TextView
        val ivLike: ImageView
        val photo1: ImageView
        val photo2: ImageView
        val title: TextView
        val rootView: ConstraintLayout

        init {
            // Define click listener for the ViewHolder's View.
            rootView = view.findViewById(R.id.root_group)
            title = view.findViewById(R.id.tvTitle)
            likeCount = view.findViewById(R.id.tvLike)
            ivLike = view.findViewById(R.id.ivLike)
            photo1 = view.findViewById(R.id.ivImage1)
            photo2 = view.findViewById(R.id.ivImage2)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_group, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        //  viewHolder.textView.text = dataSet[position]
        viewHolder.apply {
            title.text = dataSet[position]
        }
        if (position == 0) {
            viewHolder.apply {
                photo1.visible()
                photo2.visible()
            }
        } else {
            viewHolder.apply {
                photo1.gone()
                photo2.gone()
            }
        }
        viewHolder.apply {
            ivLike.setOnClickListener {
                ivLike.setImageResource(R.drawable.ic_red_like);
                likeCount.visible()
            }

            rootView.setOnClickListener(View.OnClickListener {
                context.startActivity(Intent(context, VenueDetailActivity::class.java))
            })
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}