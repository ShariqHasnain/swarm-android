package com.msha3lil.android.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.FriendListAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.activity_your_friends.*

class YourFriendsActivity : AppCompatActivity(), ItemClickListener {
    private var adapter: FriendListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_your_friends)

        adapter = FriendListAdapter(arrayListOf("Ahmed","Ali","Kaled","Mohammad"), this)
        friendsList.adapter = adapter
        backBtn.setOnClickListener{
            finish()
        }
        searchEt.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.toString()?.let {
                    adapter?.filter?.filter(s)
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })


    }

    override fun clicked(data: String) {

    }
}