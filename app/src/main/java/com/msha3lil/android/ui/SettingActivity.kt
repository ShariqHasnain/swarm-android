package com.msha3lil.android.ui

import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView.OnItemClickListener
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.FriendListAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import com.msha3lil.android.ui.adapter.StickerAdapter
import com.msha3lil.android.ui.category.CategoryActivity
import com.msha3lil.android.ui.setting.ManualSettingsActivity
import com.msha3lil.android.ui.sticker.StickerActivity
import com.msha3lil.android.utils.extention.gone
import com.msha3lil.android.utils.extention.invisible
import com.msha3lil.android.utils.extention.visible
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.activity_setting.ivBack
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.title_toolbar.*
import java.io.FileNotFoundException
import java.io.InputStream


class SettingActivity : AppCompatActivity(), ItemClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }

        if (intent.getBooleanExtra("Setting", false)) {
            tvPendingRequest.gone()
            tvMessage.gone()
            tvFriends.visible()
            tvEdit.visible()
            showMap(true)
            llUser.setOnClickListener {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
            }
            llCheckIn.setOnClickListener {
                startActivity(Intent(this, CheckInActivity::class.java))
            }
        } else {
            if (intent.getBooleanExtra("Friend", false)) {
                tvPendingRequest.gone()
                tvMessage.visible()
                tvFriends.gone()
                tvEdit.gone()
            } else {

                tvPendingRequest.visible()
            }

            setSupportActionBar(findViewById(R.id.toolbar))
            ivSetting.gone()
            tvMessage.visible()
            tvFriends.gone()
            tvEdit.gone()
            showMap(true)
            llCheckIn.setOnClickListener {
                val intent = Intent(this, CheckInActivity::class.java)
                intent.putExtra("name",tvUserName.text.toString())
                startActivity(intent)
            }
        }
        val adpter = StickerAdapter(this, arrayOf("", "", "", "", "", "", "", ""))
        stickerGridView.setAdapter(adpter)
        //item click listner
        stickerGridView.onItemClickListener = OnItemClickListener { parent, view, position, id ->

            startActivity(Intent(this, StickerActivity::class.java))
        }
        ivSetting.setOnClickListener {
            startActivity(Intent(this, ManualSettingsActivity::class.java))
        }


        llMayorships.setOnClickListener {
            startActivity(Intent(this, MayorshipsActivity::class.java))
        }
        llCategories.setOnClickListener {
            startActivity(Intent(this, CategoryActivity::class.java))
        }
        llSavePlace.setOnClickListener {
            startActivity(Intent(this, PlaceActivity::class.java))
        }
        llVisitedPlace.setOnClickListener {
            startActivity(Intent(this, PlaceActivity::class.java))
        }
        tvCheckInNow.setOnClickListener {
            startActivity(Intent(this, CheckPlaceActivity::class.java))
        }
        tvAddFriends.setOnClickListener {
            startActivity(Intent(this, AddFriendActivity::class.java))
        }
        tvFriends.setOnClickListener {
            startActivity(Intent(this, AddFriendActivity::class.java))
        }

        rlFriendList.adapter = FriendListAdapter(arrayListOf("M. Shariq Hasnain"), this)

        tvFriends1.setOnClickListener {
            startActivity(Intent(this, YourFriendsActivity::class.java))
        }

        tvEdit.setOnClickListener {
            startActivity(Intent(this, EditProfileActivity::class.java))
        }

        llStreaks.setOnClickListener {
            startActivity(Intent(this, CheckInStreakActivity::class.java))

        }

        tvMessage.setOnClickListener {
            startActivity(Intent(this, MsgActivity::class.java))
        }
    }

    override fun clicked(data: String) {
        var intent = Intent(this, SettingActivity::class.java)
        intent.putExtra("Setting", false)
        intent.putExtra("Friend", true)
        startActivity(intent)
    }

    private fun showMap(flag: Boolean) {

        if (flag) {
            rlCard.invisible()
            frgMap.view?.visible()
        } else {
            rlCard.visible()
            frgMap.view?.gone()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.friend_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.getItemId()

        if (id == R.id.unfriend) {
            showDialog()
            return true
        }

        return super.onOptionsItemSelected(item)

    }

    private fun showDialog() {
        val builder1 = AlertDialog.Builder(this)
        builder1.setMessage("Are you sure you want to remove this friend?")
        builder1.setCancelable(true)

        builder1.setPositiveButton(
            "Yes",
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        builder1.setNegativeButton(
            "No",
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        val alert11: AlertDialog = builder1.create()
        alert11.show()
    }

    val PICK_IMAGE = 1

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            var selectedImageUri: Uri? = null
            if (data != null) {
                selectedImageUri = data.data
                val `in`: InputStream?
                try {
                    `in` = contentResolver.openInputStream(selectedImageUri!!)
                    val selected_img = BitmapFactory.decodeStream(`in`)
//                    llUser.setImageBitmap(selected_img)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "An error occured!", Toast.LENGTH_LONG).show()
                }
            }


        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}