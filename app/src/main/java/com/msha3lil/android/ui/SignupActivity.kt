package com.msha3lil.android.ui

import android.R.attr
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.title_toolbar.*
import androidx.core.app.ActivityCompat.startActivityForResult
import android.graphics.Bitmap

import android.R.attr.data
import android.graphics.BitmapFactory

import android.R.attr.bitmap

import android.provider.MediaStore

import android.R.attr.data
import android.database.Cursor
import android.net.Uri
import android.widget.Toast

import android.R.attr.data
import java.io.FileNotFoundException
import java.io.InputStream


class SignupActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        initUi()
    }
    private fun initUi() {
        tvTbTitle.text="Swarm"
        ivBack.setOnClickListener {
            finish()
        }
        tvTbSave.setOnClickListener {
            startActivity(Intent(this, AddFriendActivity::class.java))
        }
        tvLogin.setOnClickListener{
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
        }
    }

    val PICK_IMAGE = 1

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            var selectedImageUri: Uri? = null
            if (data != null) {
                selectedImageUri = data.data
                val `in`: InputStream?
                try {
                    `in` = contentResolver.openInputStream(selectedImageUri!!)
                    val selected_img = BitmapFactory.decodeStream(`in`)
                    tvLogin.setImageBitmap(selected_img)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "An error occured!", Toast.LENGTH_LONG).show()
                }
            }


        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}