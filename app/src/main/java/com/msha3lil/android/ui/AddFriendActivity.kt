package com.msha3lil.android.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.msha3lil.android.MainActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.ContactListAdapter
import com.msha3lil.android.ui.models.Contact
import kotlinx.android.synthetic.main.activity_add_friend.*


class AddFriendActivity : AppCompatActivity() {

    private lateinit var adapter: ContactListAdapter
    private val arrayList = arrayListOf<Contact>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_friend)
        initUi()

        etSearch.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(::adapter.isInitialized)
                    adapter.filter.filter(s)
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
    }

    private fun initUi() {
        btnSkip.setOnClickListener {
            val a = Intent(this, MainActivity::class.java)
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(a)
        }

        llContacts.setOnClickListener {
            val hasContactPermission = ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED
            if (!hasContactPermission) {
                askPermission()
            } else {
                tapToFindContactsText.visibility = View.GONE
                contactsRv.visibility = View.VISIBLE
                readContacts()
            }
        }
    }

    private fun askPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.READ_CONTACTS),
            100
        )
    }

    private fun readContacts() {
        arrayList.clear()
        val contentResolver = contentResolver
        val cursor: Cursor? =
            contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        cursor?.let {
            if (cursor.moveToFirst()) {
                do {
                    try {
                        val name = it.getString(it.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME))
                        val number = it.getString(it.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY))
                        arrayList.add(Contact(name,number))
                    } catch (e: Exception) {

                    }
                } while (cursor.moveToNext())
                adapter = ContactListAdapter(arrayList)
                contactsRv.adapter = adapter
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        readContacts()
    }
}