package com.msha3lil.android.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.TipAdapter
import kotlinx.android.synthetic.main.activity_page.*
import kotlinx.android.synthetic.main.activity_tip.*
import kotlinx.android.synthetic.main.activity_tip.ivBack

class TipActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tip)


        initUi()
    }

    private fun initUi() {
        var adapter = TipAdapter(arrayOf("", ""))
        rlInbox.adapter = adapter
        ivBack.setOnClickListener {
            finish()
        }
    }
}