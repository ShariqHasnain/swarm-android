package com.msha3lil.android.ui.category

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.simple_toolbar.*
import kotlinx.android.synthetic.main.simple_toolbar.ivBack
import kotlinx.android.synthetic.main.simple_toolbar.tvTbTitle

class CategoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)


        tvTbTitle.text="Categories"
//        val fab: FloatingActionButton = findViewById(R.id.fab)
//
//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }
        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
    }
}