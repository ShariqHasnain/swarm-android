package com.msha3lil.android.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.ItemClickListener
import com.msha3lil.android.ui.adapter.RoadMapAdapter
import com.msha3lil.android.ui.models.RoadMap
import kotlinx.android.synthetic.main.activity_check_in.*
import kotlinx.android.synthetic.main.simple_toolbar.*
import java.util.*

class CheckInActivity : AppCompatActivity(), ItemClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in)

        var adapter = RoadMapAdapter(getData(), this)
        rlRoadMap.adapter = adapter
        if (intent.hasExtra("name"))
            tvTbTitle.text = intent.getStringExtra("name")
        else
            tvTbTitle.text = "Check-ins"
        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
    }

    override fun clicked(data: String) {
        startActivity(Intent(this, VenueDetailActivity::class.java))
    }

    private fun getData(): ArrayList<RoadMap> {
        val arrayList = arrayListOf<RoadMap>()
        arrayList.add(RoadMap("Mashroom", true))
        arrayList.add(RoadMap("Kitkat", true))
        arrayList.add(RoadMap("Oreo", true))
        return arrayList
    }


}