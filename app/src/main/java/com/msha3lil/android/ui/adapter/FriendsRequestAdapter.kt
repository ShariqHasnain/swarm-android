package com.msha3lil.android.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R

class FriendsRequestAdapter(
    private val dataSet: ArrayList<String>,
    private val listener: ItemClickListener
) :
    RecyclerView.Adapter<FriendsRequestAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val acceptRequest: TextView = view.findViewById(R.id.tvAccept)
        val declineRequest: TextView = view.findViewById(R.id.tvDecline)

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_friends_requests, viewGroup, false)
        view.setOnClickListener {
            listener.clicked("")
        }
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        //  viewHolder.textView.text = dataSet[position]
        viewHolder.acceptRequest.setOnClickListener {
            dataSet.remove(dataSet[position])
            notifyDataSetChanged()
        }
        viewHolder.declineRequest.setOnClickListener {
            dataSet.remove(dataSet[position])
            notifyDataSetChanged()
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}