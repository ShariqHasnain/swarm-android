package com.msha3lil.android.ui.adapter

interface ItemClickListener {

    fun clicked(data: String)
}