package com.msha3lil.android.ui.setting

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.MainActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.AuthActivity
import com.msha3lil.android.ui.LoginActivity
import kotlinx.android.synthetic.main.activity_setting_manual.*
import kotlinx.android.synthetic.main.simple_toolbar.*

class ManualSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_manual)

        tvTbTitle.text="Setting"

        initUi()
    }
    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
        tvLogOut.setOnClickListener {
            val a = Intent(this, AuthActivity::class.java)
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(a)
            finish()
        }
    }

}