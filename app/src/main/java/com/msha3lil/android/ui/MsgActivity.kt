package com.msha3lil.android.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.FriendsRequestAdapter
import com.msha3lil.android.ui.adapter.InboxAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import com.msha3lil.android.ui.chat.ChatActivity
import kotlinx.android.synthetic.main.activity_msg.*

class MsgActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_msg)

        initUi(this)

    }

    private fun initUi(context: Context) {
        ivBack.setOnClickListener {
            finish()
        }
        rlFriendsRequsts.adapter =
            FriendsRequestAdapter(arrayListOf("", "", "", ""), object : ItemClickListener {
                override fun clicked(data: String) {
                    var intent = Intent(context, SettingActivity::class.java)
                    intent.putExtra("Setting", false)
                    startActivity(intent)

                }
            })

        rlInbox.adapter = InboxAdapter(arrayOf(""), object : ItemClickListener {
            override fun clicked(data: String) {
                var intent = Intent(context, ChatActivity::class.java)
                intent.putExtra("Setting", false)
                startActivity(intent)
            }
        })

        ivBack.setOnClickListener {
            finish()
        }
        newMsgIv.setOnClickListener {
            var intent = Intent(context, NewMessageActivity::class.java)
            startActivity(intent)
        }
    }

}