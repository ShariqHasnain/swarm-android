package com.msha3lil.android.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R
import com.msha3lil.android.ui.models.RoadMap

class RoadMapAdapter(
    private val dataSet: ArrayList<RoadMap>,
    private val listener: ItemClickListener,
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class RoadMapSelectionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleView: TextView = view.findViewById(R.id.tvTitle)
        val tickImage: ImageView = view.findViewById(R.id.ivtick)
        val crossImage: ImageView = view.findViewById(R.id.ivCross)

    }

    class RoadMapViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleView: TextView = view.findViewById(R.id.tvTitle)

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // Create a new view, which defines the UI of the list item
        var view: View? = null
        return if (viewType == 0) {
            view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_roadmap_selection, viewGroup, false)
            view?.setOnClickListener {
                listener.clicked("")
            }
            RoadMapSelectionViewHolder(view)
        } else {
            view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_roadmap, viewGroup, false)
            view?.setOnClickListener {
                listener.clicked("")
            }
            RoadMapViewHolder(view)
        }

    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is RoadMapSelectionViewHolder) {
            viewHolder.titleView.text = dataSet[position].title
            viewHolder.tickImage.setOnClickListener {
                dataSet.removeAt(position)
                notifyDataSetChanged()
            }
            viewHolder.crossImage.setOnClickListener {
                dataSet.removeAt(position)
                notifyDataSetChanged()
            }
        } else if (viewHolder is RoadMapViewHolder) {
            viewHolder.titleView.text = dataSet[position].title
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    override fun getItemViewType(position: Int): Int {
        return if(dataSet[position].isAdded)
            1
        else
            0
    }


}