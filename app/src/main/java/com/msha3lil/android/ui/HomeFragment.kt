package com.msha3lil.android.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.ItemClickListener
import com.msha3lil.android.ui.adapter.RoadMapAdapter
import com.msha3lil.android.ui.category.CategoryActivity
import com.msha3lil.android.ui.models.RoadMap
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment(), ItemClickListener, OnMapReadyCallback,
    GoogleMap.OnMapClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private lateinit var mMap: GoogleMap


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())

        var adapter = RoadMapAdapter(getData(), this)
        rlRoadMap.adapter = adapter


        mapView.setOnClickListener {
            openPlaceActivity()
        }
        llVisitedPlace.setOnClickListener {
            openPlaceActivity()
        }
        llSavePlace.setOnClickListener {
            openPlaceActivity()
        }

        llCategories.setOnClickListener {
            startActivity(Intent(activity, CategoryActivity::class.java))
        }

        placeholder.setOnClickListener {
            openPlaceActivity()
        }

    }

    private fun getData(): ArrayList<RoadMap> {
        val arrayList = arrayListOf<RoadMap>()
        arrayList.add(RoadMap("Four Seasons Hotel", false))
        arrayList.add(RoadMap("Four Seasons Hotel", false))
        arrayList.add(RoadMap("Four Seasons Hotel", true))
        arrayList.add(RoadMap("Four Seasons Hotel", true))
        arrayList.add(RoadMap("Four Seasons Hotel", true))
        arrayList.add(RoadMap("Four Seasons Hotel", true))
        return arrayList
    }

    private fun openPlaceActivity() {
        startActivity(Intent(activity, PlaceActivity::class.java))
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun clicked(data: String) {
        startActivity(Intent(requireActivity(), VenueDetailActivity::class.java))
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(
            MarkerOptions()
                .position(sydney)
                .title("Marker in Sydney")
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            if (location == null) {
                //    LogUtil.w(TAG, "onSuccess:null")
                return@addOnSuccessListener
            }

            val latLng = LatLng(location.latitude, location.longitude)

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))

        }
    }

    override fun onMapClick(p0: LatLng) {
        Toast.makeText(activity, "dasd", Toast.LENGTH_SHORT).show();
    }
}