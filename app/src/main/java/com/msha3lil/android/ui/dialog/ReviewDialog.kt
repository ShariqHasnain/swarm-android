package com.msha3lil.android.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.NonNull
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.msha3lil.android.R
import com.msha3lil.android.utils.extention.visible
import kotlinx.android.synthetic.main.dialog_review.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class ReviewDialog : DialogFragment() {
    @NonNull
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                Color
                    .TRANSPARENT
            )
        )
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomUp: Animation = AnimationUtils.loadAnimation(
            activity,
            R.anim.bounch
        )

        constraintLayout.startAnimation(bottomUp)
        constraintLayout.visibility = View.VISIBLE

        tvCheckIn.setOnClickListener {
            dismiss()
            var dialog = CollectProgressDialog()
            dialog.show(requireActivity().supportFragmentManager, "ViewPrescriptionDialog")
        }
        tvUpvote.setOnClickListener {
            tvUpvote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vote_green, 0, 0, 0);
            tvUpvote.setTextColor(Color.GREEN)
        }

    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }
}