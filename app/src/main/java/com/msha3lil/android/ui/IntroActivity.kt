package com.msha3lil.android.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.msha3lil.android.R
import com.msha3lil.android.data.OnBoardingItem
import com.msha3lil.android.ui.adapter.OnBoardingAdapter
import com.msha3lil.android.utils.extention.gone
import com.msha3lil.android.utils.extention.visible
import kotlinx.android.synthetic.main.activity_intro.*
import java.util.*

class IntroActivity : AppCompatActivity() {

    private var onboardingAdapter: OnBoardingAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        initUI()
    }

    fun openWithPermission() = runWithPermissions(
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) {
        startActivity(Intent(applicationContext, AddFriendActivity::class.java))
        finish()
    }

    private fun initUI() {
        setOnboardingItem()

        onboardingViewPager.adapter = onboardingAdapter



        setOnboadingIndicator()
        setCurrentOnboardingIndicators(0)
        onboardingViewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentOnboardingIndicators(position)
            }
        })

        btnNext.setOnClickListener(View.OnClickListener {
            if (onboardingViewPager.currentItem + 1 < onboardingAdapter!!.itemCount) {
                onboardingViewPager.currentItem = onboardingViewPager.currentItem + 1
            } /*else {
                startActivity(Intent(applicationContext, HomeActivity::class.java))
                finish()
            }*/
        })
        tvEnableLocation.setOnClickListener {
            openWithPermission()
        }
        tvDismiss.setOnClickListener {
            startActivity(Intent(applicationContext, AddFriendActivity::class.java))
            finish()
        }
        introBack.setOnClickListener {
            onboardingViewPager.currentItem = onboardingViewPager.currentItem - 1
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setCurrentOnboardingIndicators(index: Int) {
        val childCount: Int = layoutOnboardingIndicators.getChildCount()
        for (i in 0 until childCount) {
            val imageView = layoutOnboardingIndicators.getChildAt(i) as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.onboarding_indicator_active
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.onboarding_indicator_inactive
                    )
                )
            }
        }
        if (index == 0) {
            introBack.gone()
        } else {
            introBack.visible()
        }

        if (index == 3) {
            btnNext.gone()
            tvEnableLocation.visible()
            tvDismiss.visible()
        } else {
            btnNext.visible()
            tvEnableLocation.gone()
            tvDismiss.gone()
        }

    }

    private fun setOnboardingItem() {
        val onBoardingItems: MutableList<OnBoardingItem> = ArrayList<OnBoardingItem>()
        val itemFastFood = OnBoardingItem()
        itemFastFood.title = ("Choose your meal")
        itemFastFood.description =
            ("Keep track of the places you visit and get helpful insights about your habits.")
        itemFastFood.image = (R.drawable.intro1)
        val itemPayOnline = OnBoardingItem()
        itemPayOnline.title = ("Choose your payment")
        itemPayOnline.description =
            ("Collect different types of places. Museum? Karaoke bar? Now try somewhere new!")
        itemPayOnline.image = (R.drawable.intro2)
        val itemEatTogether = OnBoardingItem()
        itemEatTogether.title = ("Fast delivery")
        itemEatTogether.description =
            ("Never ask \"What's the name of that place?\" again. Take a scroll down memory lane.")
        itemEatTogether.image = (R.drawable.intro3)
        val itemDayAndNight = OnBoardingItem()
        itemDayAndNight.title = ("Day and Night")
        itemDayAndNight.description =
            ("Allow Swarm to access your location?  Find nearby friends and get check-in reminders. Set Swarm's location permission to \"Always Allow\" so your can receive notifications when you're out exploring and the app is running in the background. We take your privacy very seriously, and encourage you to review our Privacy Policy")
        itemDayAndNight.image = (R.drawable.intro4)
        onBoardingItems.add(itemFastFood)
        onBoardingItems.add(itemPayOnline)
        onBoardingItems.add(itemEatTogether)
        onBoardingItems.add(itemDayAndNight)
        onboardingAdapter = OnBoardingAdapter(onBoardingItems)
    }

    private fun setOnboadingIndicator() {
        val indicators = arrayOfNulls<ImageView>(
            onboardingAdapter!!.itemCount
        )
        val layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i]!!.setImageDrawable(
                ContextCompat.getDrawable(
                    applicationContext, R.drawable.onboarding_indicator_inactive
                )
            )
            indicators[i]!!.layoutParams = layoutParams
            layoutOnboardingIndicators.addView(indicators[i])
        }
    }
}