package com.msha3lil.android.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.utils.extention.gone
import com.msha3lil.android.utils.extention.isVisible
import com.msha3lil.android.utils.extention.visible
import kotlinx.android.synthetic.main.activity_page.*

class PageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page)

        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
        llFriendCheckIn.setOnClickListener {
            startActivity(Intent(this, AreHereActivity::class.java))
        }
        tvTip.setOnClickListener {
            startActivity(Intent(this, WriteTipActivity::class.java))
        }
        tvRate.setOnClickListener {
            if (clRate.isVisible())
                clRate.gone()
            else {
                clRate.visible()
            }
        }
        ivLike.setOnClickListener {
            changeRateIcons(0)
        }
        ivEmoji.setOnClickListener {
            changeRateIcons(1)
        }
        ivDislike.setOnClickListener {
            changeRateIcons(2)
        }
        tvSave.setOnClickListener {
            tvSave.isSelected = !tvSave.isSelected
            if (tvSave.isSelected)
                tvSave.text = "Saved"
            else
                tvSave.text = "Save"
        }
        tvAddHours.setOnClickListener {
            showHoursDialog()
        }
        tvAddPhone.setOnClickListener {
            showAddNumberDialog()
        }
        tvAddWebsite.setOnClickListener {
            showAddWebsiteDialog()
        }

        ivMenu.setOnClickListener {
            showPopMenu()
        }
        ivShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            val shareBody = "Four seasons hotel"
            intent.putExtra(Intent.EXTRA_TEXT, shareBody)
            startActivity(Intent.createChooser(intent, "Share this place"))
        }
    }

    private fun showPopMenu() {
        val popup = PopupMenu(this@PageActivity, ivMenu)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(com.msha3lil.android.R.menu.page_menu, popup.menu)

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener {
            if (it.itemId == R.id.check_in) {
                startActivity(Intent(this@PageActivity, CheckOutActivity::class.java))
            }
            true
        }
        popup.show()
    }

    private fun showHoursDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.add_hours_dialog)
        dialog.show()
        dialog.findViewById<TextView>(R.id.tvSave).setOnClickListener {
            val startTime = dialog.findViewById<EditText>(R.id.startTime).text.toString()
            val endTime = dialog.findViewById<EditText>(R.id.endTime).text.toString()
            tvAddHours.text = "$startTime - $endTime"
            tvAddHours.isEnabled = false
            dialog.dismiss()
        }
        val window: Window? = dialog.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun showAddNumberDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.add_phone_number_dialog)
        dialog.show()
        dialog.findViewById<TextView>(R.id.tvSave).setOnClickListener {
            val number = dialog.findViewById<EditText>(R.id.mobileNumber).text.toString()
            tvAddPhone.text = "$number"
            tvAddPhone.isEnabled = false
            dialog.dismiss()
        }
        val window: Window? = dialog.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun showAddWebsiteDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.add_website_dialog)
        dialog.show()
        dialog.findViewById<TextView>(R.id.tvSave).setOnClickListener {
            val number = dialog.findViewById<EditText>(R.id.website).text.toString()
            tvAddWebsite.text = "$number"
            tvAddWebsite.isEnabled = false
            dialog.dismiss()
        }
        val window: Window? = dialog.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }


    private fun changeRateIcons(id: Int) {
        when (id) {
            0 -> {
                tvRate.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_like_red, 0, 0)
                ivLike.setImageResource(R.drawable.ic_like_red);
                ivEmoji.setImageResource(R.drawable.ic_emoji_grey);
                ivDislike.setImageResource(R.drawable.ic_dislike_grey);
            }
            1 -> {
                tvRate.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_emoji_black, 0, 0)
                ivLike.setImageResource(R.drawable.ic_like_grey);
                ivEmoji.setImageResource(R.drawable.ic_emoji_black);
                ivDislike.setImageResource(R.drawable.ic_dislike_grey);
            }
            2 -> {
                tvRate.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_dislike_black, 0, 0)
                ivLike.setImageResource(R.drawable.ic_like_grey);
                ivEmoji.setImageResource(R.drawable.ic_emoji_grey);
                ivDislike.setImageResource(R.drawable.ic_dislike_black);
            }
        }

    }
}