package com.msha3lil.android.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import kotlinx.android.synthetic.main.activity_auth.*


class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        initUi()
    }

    private fun initUi() {
        tvLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
        ivEmail.setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
        }
        val bottomUp: Animation = AnimationUtils.loadAnimation(
            this,
            R.anim.bottom_up
        )
        val hiddenPanel = findViewById<RelativeLayout>(R.id.rrBottom) as ViewGroup
        hiddenPanel.startAnimation(bottomUp)
        hiddenPanel.visibility = View.VISIBLE

    }
}