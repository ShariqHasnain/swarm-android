package com.msha3lil.android.ui

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import androidx.annotation.RequiresApi
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.CheckPlaceAdapter
import com.msha3lil.android.ui.adapter.GroupAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.home_toolbar.*

class SearchActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        clickListener()
        etSearch.focusable = View.FOCUSABLE
        etSearch.inputType = InputType.TYPE_CLASS_TEXT
        etSearch.isFocusableInTouchMode = true
        etSearch.requestFocus()
    }

    private fun clickListener() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(s.isNullOrEmpty()){
                    noContentContainer.visibility = View.VISIBLE
                    content_container.visibility = View.GONE
                } else {
                    noContentContainer.visibility = View.GONE
                    content_container.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        LytFriends.setOnClickListener {
            startActivity(Intent(this, SearchDetailActivity::class.java))
        }

        LytPlaces.setOnClickListener {
            startActivity(Intent(this, SearchDetailActivity::class.java))
        }
    }
}