package com.msha3lil.android.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.AreHereAdapter
import com.msha3lil.android.ui.adapter.GroupAdapter
import kotlinx.android.synthetic.main.activity_leader_board.*

class AreHereActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_are_here)

        initUi()

    }
    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
        var adapter= AreHereAdapter(arrayOf("Mohammad", "Ali", "Saleem"))
        rlFriends.adapter=adapter

    }
}