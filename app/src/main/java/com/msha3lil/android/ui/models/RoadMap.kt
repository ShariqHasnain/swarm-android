package com.msha3lil.android.ui.models

data class RoadMap(val title: String, val isAdded: Boolean)
