package com.msha3lil.android.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.NonNull
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.msha3lil.android.R
import com.msha3lil.android.utils.extention.gone
import com.msha3lil.android.utils.extention.visible
import kotlinx.android.synthetic.main.dialog_collect_progress.*
import kotlinx.android.synthetic.main.dialog_collect_progress.constraintLayout
import kotlinx.android.synthetic.main.dialog_collect_progress.gif
import kotlinx.android.synthetic.main.dialog_collect_progress.tvCheckIn
import kotlinx.android.synthetic.main.dialog_more_checking.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MoreCheckingDialog : DialogFragment() {
    @NonNull
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                Color
                    .TRANSPARENT
            )
        )
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_more_checking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomUp: Animation = AnimationUtils.loadAnimation(
            activity,
            R.anim.bounch
        )

        constraintLayout.startAnimation(bottomUp)
        constraintLayout.visibility = View.VISIBLE
        lifecycleScope.launch {
            delay(1000)
            gif.visible()
        }
        tvName1.setOnClickListener {
            dismiss()
            var dialog = ReviewDialog()
            dialog.show(requireActivity().supportFragmentManager, "ViewPrescriptionDialog")
        }
        tvName2.setOnClickListener {
           clFirst.gone()
            clWrong.visible()
        }
        tvName3.setOnClickListener {
            clFirst.gone()
            clRight.visible()
        }

    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }
}