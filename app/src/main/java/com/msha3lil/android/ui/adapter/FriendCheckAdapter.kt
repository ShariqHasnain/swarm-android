package com.msha3lil.android.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msha3lil.android.R

class FriendCheckAdapter(
    private val dataSet: ArrayList<String>,
    private val listener: ItemClickListener
) :
    RecyclerView.Adapter<FriendCheckAdapter.ViewHolder>(),Filterable {
    var datasetFiltered = dataSet
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivCheck: ImageView = view.findViewById(R.id.ivCheck)
        val tvTitle: TextView = view.findViewById(R.id.tvTitle)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_friend_check, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.tvTitle.text = datasetFiltered[position]
        viewHolder.itemView.setOnClickListener {
            viewHolder.ivCheck.setImageResource(R.drawable.ic_enable_check)
            listener.clicked(data = datasetFiltered[position])
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint?.toString() ?: ""
                if (charString.isEmpty()) datasetFiltered = dataSet else {
                    val filteredList = arrayListOf<String>()
                    dataSet
                        .filter {
                            (it.contains(constraint!!, true))

                        }
                        .forEach { filteredList.add(it) }
                    datasetFiltered = filteredList

                }
                return FilterResults().apply { values = datasetFiltered }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                datasetFiltered = if(results?.values == null) arrayListOf()
                else
                    results.values as ArrayList<String>
                notifyDataSetChanged()
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = datasetFiltered.size

}