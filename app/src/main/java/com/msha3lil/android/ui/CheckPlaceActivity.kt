package com.msha3lil.android.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.CheckPlaceAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import kotlinx.android.synthetic.main.activity_check_place.*


class CheckPlaceActivity : AppCompatActivity(), ItemClickListener {
    private var adapter: CheckPlaceAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_place)
        toolbar.setNavigationIcon(R.drawable.ic_cross_white)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        adapter = CheckPlaceAdapter(
            arrayListOf(
                "Four Seasons Hotel",
                "Four Seasons Hotel",
                "Four Seasons Hotel",
                "Four Seasons Hotel",
                "Four Seasons Hotel",
                "Four Seasons Hotel",
                "Four Seasons Hotel",
                "Four Seasons Hotel",
                "Four Seasons Hotel",
            ), this
        )
        rlCategory.adapter = adapter

        initUi()
    }

    private fun initUi() {

    }

    override fun clicked(data: String) {

        startActivity(Intent(this, CheckOutActivity::class.java))
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.check_place_menu, menu)

        val searchItem: MenuItem? = menu?.findItem(R.id.action_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView = searchItem?.actionView as SearchView
        val searchEditText =
            searchView.findViewById<View>(R.id.search_src_text) as EditText
        searchEditText.setTextColor(ContextCompat.getColor(this, R.color.white))
        searchEditText.setHintTextColor(ContextCompat.getColor(this, R.color.white))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.trim()?.let {
                    adapter?.filter?.filter(it)
                }
                return false
            }

        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}