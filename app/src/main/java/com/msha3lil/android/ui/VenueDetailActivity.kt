package com.msha3lil.android.ui

import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.CommentAdapter
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_venue_detail.*
import kotlinx.android.synthetic.main.title_toolbar.ivBack
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.ArrayList

class VenueDetailActivity : AppCompatActivity() {

    private var adapter: CommentAdapter? = null
    private val commentsList: ArrayList<String> = arrayListOf()
    val PICK_IMAGE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venue_detail)

        initUi()
    }

    private fun initUi() {
        ivBack.setOnClickListener {
            finish()
        }
        rlDetailCard.setOnClickListener {
            startActivity(Intent(this, PageActivity::class.java))
        }
        tvTips.setOnClickListener {
            startActivity(Intent(this, WriteTipActivity::class.java))
        }
        ivLike.setOnClickListener {
            ivLike.isSelected = !ivLike.isSelected
        }
        ivEmoji.setOnClickListener {
            ivEmoji.isSelected = !ivEmoji.isSelected

        }
        ivDislike.setOnClickListener {
            ivDislike.isSelected = !ivDislike.isSelected

        }

        tvAddPhoto.setOnClickListener {
            openGallery()
        }
        addMoreImagesTv.setOnClickListener {
            openGallery()
        }
        ivImageComment.setOnClickListener {
            openGallery()
        }

        adapter = CommentAdapter(commentsList)
        rvComments.adapter = adapter
        commentsEt.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                val text = commentsEt.text.toString().trim()
                if (text.isNotEmpty()) {
                    addComment(text)
                }
                commentsEt.text.clear()
                rvComments.scrollToPosition(commentsList.size - 1)
                true
            } else false
        })

    }

    private fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
    }

    private fun addComment(text: String) {
        commentsList.add(text)
        adapter?.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            var selectedImageUri: Uri? = null
            if (data != null) {
                selectedImageUri = data.data
                val `in`: InputStream?
                try {
                    `in` = contentResolver.openInputStream(selectedImageUri!!)
                    val selectedImage = BitmapFactory.decodeStream(`in`)
                    hideNoPicsView()
                    val imageView = ImageView(this)
                    imageView.setImageBitmap(selectedImage)
                    imageView.layoutParams = LinearLayout.LayoutParams(300,300)
                    imageHolder.addView(imageView)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "An error occured!", Toast.LENGTH_LONG).show()
                }
            }


        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun hideNoPicsView() {
        tvTitle3.visibility = View.GONE
        tvDes3.visibility = View.GONE
        tvAddPhoto.visibility = View.GONE
        addMoreImagesTv.visibility = View.VISIBLE
    }
}