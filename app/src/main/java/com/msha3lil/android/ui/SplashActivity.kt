package com.msha3lil.android.ui


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.msha3lil.android.R
import com.msha3lil.android.utils.Constant


class SplashActivity : AppCompatActivity() {

    private var handler: Handler = Handler()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        var isUserLoggedIn = sharedPref.getBoolean(Constant.IS_LOGIN, Constant.DEFAULT_BOOLEAN)

        handler.postDelayed(Runnable {
            if(isUserLoggedIn)
            {
                moveHomeScreen()
            }
            else{
                moveLoginScreen()
            }
        }, 3000)

    }


    private fun moveLoginScreen() {
        startActivity(Intent(this, AuthActivity::class.java))
        finish()
    }

    private fun moveHomeScreen() {
//        val intent = Intent(this, DoctorHomeActivity::class.java)
//        startActivity(intent)
//        finish()
    }


}