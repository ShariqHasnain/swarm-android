package com.msha3lil.android.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.*
import com.msha3lil.android.ui.chat.ChatActivity
import com.msha3lil.android.ui.dialog.CollectProgressDialog
import com.msha3lil.android.ui.dialog.LeaderboardDialog
import kotlinx.android.synthetic.main.activity_leader_board.*

class LeaderBoardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leader_board)

        initUi(this)

    }
    private fun initUi(context: Context) {
        ivBack.setOnClickListener {
            finish()
        }
        rlRanks.adapter= RankFriendsAdapter(arrayOf("","",""),object : ItemClickListener {
            override fun clicked(data: String) {
                var intent= Intent(this@LeaderBoardActivity, SettingActivity::class.java)
                intent.putExtra("Friend",true)
                startActivity(intent)
            }
        })

        rlFriends.adapter= FriendSuggestAdapter(arrayOf("",""),object : ItemClickListener {
            override fun clicked(data: String) {
                var intent= Intent(this@LeaderBoardActivity, SettingActivity::class.java)
                intent.putExtra("Friend",true)
                startActivity(intent)
            }
        })
        clBottom.setOnClickListener {
            var dialog = LeaderboardDialog()
            dialog.show(supportFragmentManager, "ViewPrescriptionDialog")
        }

        tvTbSave.setOnClickListener {
            startActivity(Intent(this, AddFriendActivity::class.java))
        }
    }
}