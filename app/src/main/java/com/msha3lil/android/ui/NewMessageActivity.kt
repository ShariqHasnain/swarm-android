package com.msha3lil.android.ui

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.chip.Chip
import com.msha3lil.android.R
import com.msha3lil.android.ui.adapter.FriendCheckAdapter
import com.msha3lil.android.ui.adapter.ItemClickListener
import kotlinx.android.synthetic.main.activity_new_message.*

class NewMessageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_message)

        rvFriendsList.adapter = FriendCheckAdapter(
            arrayListOf("Mohammad", "Ali", "Ahmed", "Adnan"),
            object : ItemClickListener {
                override fun clicked(data: String) {
                    val chip = chipGroup.findViewWithTag<Chip>(data)
                    if (chip == null) {
                        val chip = Chip(this@NewMessageActivity)
                        chip.text = data
                        chip.tag = data
                        chip.setTextColor(ContextCompat.getColor(this@NewMessageActivity,R.color.white))
                        chip.chipBackgroundColor = ContextCompat.getColorStateList(this@NewMessageActivity,R.color.primary)
                        chipGroup.addView(chip)
                    }
                    if (chipGroup.childCount > 0) {
                        selectPeopleTv.visibility = View.GONE
                        clBottom.visibility = View.VISIBLE
                    }
                    else {
                        selectPeopleTv.visibility = View.VISIBLE
                        clBottom.visibility = View.GONE
                    }

                }
            })

        backBtn.setOnClickListener{
           finish()
        }
    }
}