package com.msha3lil.android.utils

import android.content.SharedPreferences


class PrefManager (
    private val preferences: SharedPreferences
) {

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return preferences.getBoolean(key, defaultValue)
    }

    fun putBoolean(key: String, value: Boolean) {
        try {
            println("$key $value")
            preferences.edit().putBoolean(key, value).apply()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getString(key: String, defaultValue: String): String {
        var token = preferences.getString(key, defaultValue)!!
        return token
    }

    fun putString(key: String, value: String) {
        try {
            println("$key $value")
            preferences.edit().putString(key, value).apply()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
    fun remove(key: String) {
        try {
            println("$key")
            preferences.edit().remove(key).apply()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun clearAllPreferences() {
        preferences.edit().clear().apply()
    }


}