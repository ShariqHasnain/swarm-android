package com.msha3lil.android.data

data class OnBoardingItem (
    var image: Int = 0,
    var title: String? = null,
    var description: String? = null
)